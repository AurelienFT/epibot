import random
import asyncio
import aiohttp
import json
from discord import Game
from discord.ext.commands import Bot
from discord.ext import commands
from discord.utils import get
import epibotmodule
import mysql.connector
import requests
import urllib.parse

class EpiBot:
    def __init__(self):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="",
            database="epibot"
        )
        self.mycursor = self.mydb.cursor()
        BOT_PREFIX = ("?", "!")
        TOKEN = "NTk3NTQ2ODk2NDk5ODAyMTUx.XSJr1A.TdrkDArA0RamfdBuNVbiZJXtfks"  # Get at discordapp.com/developers/applications/me
        client = Bot(command_prefix=BOT_PREFIX)
        self.client = client
        
        @client.event
        async def on_ready():
            await client.change_presence(activity=Game(name="with humans"))
            print("Logged in as " + client.user.name)
            self.epibotmodules = []
            self.epibotmodules.append(epibotmodule.seekmate.SeekMate())
            for module in self.epibotmodules:
                await module.initialization(self.client)
            #await find_id_seek_mate()

        @client.command(pass_context=True)
        async def rank(context, mail, year, city):
            member = context.message.author
            print(str(context.message.author.id) + " mail's is " + str(mail))
            #url = "https://roslyn.epi.codes/trombi/api.php?version=2&action=profile&q=" + urllib.parse.quote(mail)
            #print(url)
            #r = requests.get(url = url)
            #print(r.text)
            #data = r.json()
            #print(data)
            role1 = get(member.server.roles, name=year)
            await context.message.author.add_roles(member, role1)
            role2 = get(member.server.roles, name=city)
            await context.message.author.add_roles(member, role2)
            await context.channel.delete_message(context.message)
            # TO UNCOMMENT IN PROD
            #sql = "INSERT INTO user (id, email) VALUES (%s, %s)"
            #val = (context.message.author.id, mail)
            #mycursor.execute(sql, val)
            #mydb.commit()

        client.loop.create_task(self.list_servers())
        client.run(TOKEN)

    async def list_servers(self):
        await self.client.wait_until_ready()
        while not self.client.is_closed:
            print("Current servers:")
            for server in self.client.guilds:
                for role in server.roles:
                    print(role.name)
            await asyncio.sleep(600)

    async def find_id_seek_mate(self):
        for server in self.client.guilds:
            for text_channel in server.channels:
                if text_channel.name == "seek-mate":
                    seek_mate_id = text_channel.id
                    seek_mate_history = text_channel.history()

epibot = EpiBot()
